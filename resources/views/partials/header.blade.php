<div class="page-header-inner">
    <div class="page-header-inner">
        <div class="navbar-header">
            <a href="{{ url('/') }}"
               class="navbar-brand">
                @lang('quickadmin.quickadmin_title')
            </a>
<!-- <center>Bienvenu sur la Plateforme Réservation Salle de Réunion LE 15</center>-->
        </div>
        <a href="javascript:;"
           class="menu-toggler responsive-toggler"
           data-toggle="collapse"
           data-target=".navbar-collapse">
        </a>

        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li>
                  <!-- <a href="https://quickadminpanel.com" target="_blank">Generated with QuickAdminPanel</a> -->
                  <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.qa_logout')</span>
                </a>
                </li>
            </ul>
        </div> 
    </div>
</div>


