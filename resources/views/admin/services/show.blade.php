@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.services.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.services.fields.nom')</th>
                            <td>{{ $service->nom }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.services.fields.etat')</th>
                            <td>{{ $service->etat }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.services.fields.description')</th>
                            <td>{{ $service->description}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.services.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
